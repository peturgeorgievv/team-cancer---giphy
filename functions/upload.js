import {apiKey, url} from '../apis.js';
import {putLocal} from './localStorage.js';
import {gifView} from './gifView.js';

const uploadUrl = 'https://upload.giphy.com/v1/gifs';
const myGifsLocal = 'myGifs';

const oneUp = async (el) => {
  const getGifUrl = `${url}?api_key=${apiKey}&ids=${el.id}`;
  const $div = $('#container-gifs');
  $div.empty();
  // eslint-disable-next-line max-len
  $div.append(`<h5 class="page-headers"><span>Your file was uploaded</span></h5>`);
  const promResult = await fetch(getGifUrl);
  const result = await promResult.json();
  gifView(result.data[0]);
};

const getUpload = async (data) => {
  try {
    const promRes = await fetch(`${uploadUrl}?api_key=${apiKey}`, {
      method: 'POST',
      body: data,
    });
    const result = await promRes.json();
    oneUp(result.data);
    putLocal(result.data.id, myGifsLocal);
  } catch (error) {
    console.error(error);
  }
};

const getFile = () => {
  const fileInput = $('#upload-btn');
  const file = fileInput[0].files[0];

  const formData = new FormData();
  formData.append('file', file);

  getUpload(formData);
};

const uploadGifs = () => {
  $('#upload-not-hidden').click(() => $('#upload-btn').click());
  $('#upload-btn').on('change', function(ev) {
    ev.preventDefault();
    getFile();
    $('#upload-btn').replaceWith($('#upload-btn').val('').clone(true));
  });

  // Initially cleaning browse file
  $('#upload-btn').replaceWith($('#upload-btn').val('').clone(true));
};

export {uploadGifs, myGifsLocal};
