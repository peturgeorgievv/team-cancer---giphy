const putLocal = (id, storeItem) => {
  if (!localStorage.getItem(storeItem)) {
    localStorage.setItem(storeItem, `${id}`);
  } else {
    const ids = localStorage.getItem(storeItem);
    localStorage.setItem(storeItem, `${ids},${id}`);
  }
};


const delLocal = (id, storeItem) => {
  const storageArray = localStorage.getItem(storeItem).split(',');
  const idIndex = storageArray.indexOf(id);
  // eslint-disable-next-line max-len
  const newId = [...(storageArray.slice(0, idIndex)), ...(storageArray.slice(idIndex + 1, storageArray.length))].join(',');
  localStorage.setItem(storeItem, newId);
};

export {putLocal, delLocal};
