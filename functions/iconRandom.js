// import {getRandom} from './random.js';
import {apiKey, url} from '../apis.js';
import {gifView} from './gifView.js';


const urlToUse1 = `${url}/random?api_key=${apiKey}&tag=&rating=R`;

const getRandomHidden = async () => {
  const result = await fetch(urlToUse1);
  const response = await result.json();
  gifView(response.data);
};

const refreshRandom = async () => {
  $(document).on('click', '#img-btn', () => {
    const $div = $('#container-gifs');
    $div.empty();
    $div.append(`<h5 class="page-headers"><span>Random Gif</span></h5>`);
    getRandomHidden();
  });
};

export {refreshRandom};
