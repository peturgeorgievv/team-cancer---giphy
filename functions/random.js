import {apiKey, url} from '../apis.js';
import {gifView} from './gifView.js';

const urlToUse = `${url}/random?api_key=${apiKey}&tag=&rating=G`;

const getRandom = async () => {
  const result = await fetch(urlToUse);
  const response = await result.json();
  gifView(response.data);
};
export {getRandom};
