import {apiKey, url} from '../apis.js';
import {gifView} from './gifView.js';

const populateGifs = (container, clickFn, searchItem) => {
  const $div = $(container);
  $div.empty();
  $div.append(`<h5 class="page-headers"><span>Searched Gifs</span></h5>`);
  clickFn(searchItem);
};

const getSearch = async (searchItem) => {
  const gifResult = await fetch(
  // eslint-disable-next-line max-len
      `${`${url}/search?api_key=${apiKey}&q=${searchItem}&limit=5&offset=0&rating=G&lang=en`}`
  );
  const result = await gifResult.json();
  result.data.forEach((el) => {
    gifView(el);
    $('#search').val('');
  });
};

const searching = () => {
  $('#form').on('submit',
      function(ev) {
        // console.log(ev);
        ev.preventDefault();
        const searchItem = $('#search').val().trim().split(' ').join('+');
        populateGifs('#container-gifs', getSearch, searchItem);
      });
};

export {searching};
