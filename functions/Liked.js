import {putLocal, delLocal} from './localStorage.js';
import {favouritesCall} from './favouritesCall.js';

const liked = () => {
  $(document).on('click', '.like-btn', (ev) => {
    const id = $(ev.target).parent()[0].children[3].textContent;
    // eslint-disable-next-line max-len
    const textHeader = $(ev.target).parent().parent().parent()[0].children[0].textContent;

    if ($(ev.target)[0].text === 'Like') {
      putLocal(id, 'Favourites');
      $(ev.target).html('Liked');
      if (textHeader === 'Random Gif - Please like some GIF') {
        favouritesCall();
      }
    } else {
      $(ev.target).html('Like');
      delLocal(id, 'Favourites');
      if (textHeader === 'Favourites') {
        favouritesCall();
      }
    }
  });
};

export {liked};
