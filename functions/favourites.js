import {favouritesCall} from './favouritesCall.js';

const favouritesLoad = () => {
  $('#favourites-btn').on('click', function(ev) {
    ev.preventDefault();
    favouritesCall();
  });
};

export {
  favouritesLoad,
};
