export const gifView = (el) => {
  const $div = $('#container-gifs');
  let liked = 'Like';
  if (localStorage.getItem('Favourites')) {
    // eslint-disable-next-line max-len
    liked = (localStorage.getItem('Favourites').includes(el.id))? 'Liked': 'Like';
  }

  if (el.username === '') {
    $div.append(`
        <div class="gif-container">
            <img class="gifs-size" src='${el.images.original.url}' />
            <div class="gif-details">
            <a href='${el.images.original.url}'><h5>"${el.title}"</h5></a>
            <p>Uploaded by: No Username</p>
            <a class="like-btn waves-effect waves-light btn">${liked}</a>
            <div class="liked-id" style="display: none">${el.id}</div>
            </div>
        </div>
        `);
  } else {
    $div.append(`
    <div class="gif-container">
        <img class="gifs-size" src='${el.images.original.url}' />
        <div class="gif-details">
        <a href='${el.images.original.url}'><h5>"${el.title}"</h5></a>
        <p>Uploaded by: "${el.username}"</p>
        <a class="like-btn waves-effect waves-light btn">${liked}</a>
        <div class="liked-id" style="display: none">${el.id}</div>
        </div>
    </div>
    `);
  }
};
