import {apiKey, url} from '../apis.js';
import {gifView} from './gifView.js';
import {getRandom} from './random.js';

const favouritesCall = async () => {
  const favourites = 'Favourites';
  const $div = $('#container-gifs');
  $div.empty();
  // eslint-disable-next-line max-len
  if (localStorage.getItem(favourites)) {
    // eslint-disable-next-line max-len
    const getGifUrl = `${url}?api_key=${apiKey}&ids=${localStorage.getItem(favourites)}`;
    $div.append(`<h5 class="page-headers"><span>Favourites</span></h5>`);

    const promResult = await fetch(getGifUrl);
    const result = await promResult.json();
    result.data.map((el) => gifView(el));
  } else {
    // eslint-disable-next-line max-len
    $div.append(`<h5 class="page-headers"><span>Random Gif - Please like some GIF</span></h5>`);
    getRandom();
  }
};

export {favouritesCall};
