import {apiKey, url} from '../apis.js';
import {myGifsLocal} from './upload.js';
import {gifView} from './gifView.js';

const myGifsed = async () => {
  const $div = $('#container-gifs');
  $div.empty();

  // eslint-disable-next-line max-len
  const getGifUrl = `${url}?api_key=${apiKey}&ids=${localStorage.getItem(myGifsLocal)}`;
  $div.append(`<h5 class="page-headers"><span>My Giffs</span></h5>`);

  const gifResult = await fetch(getGifUrl);
  const response = await gifResult.json();
  response.data.map((el) => gifView(el));
};

const myGifs = () => {
  $('#my-gifs-btn').on('click', function(ev) {
    ev.preventDefault();
    myGifsed();
  });
};

export {
  myGifs,
};
