import {apiKey, url} from '../apis.js';
import {gifView} from './gifView.js';


const url1 = `${url}/trending?api_key=${apiKey}&limit=5&rating=G`;

const getTrending = async () => {
  const trendRes = await fetch(url1);
  const res = await trendRes.json();
  res.data.forEach((x) => gifView(x));
};

const callbackTrend = async (ev) => {
  try {
    const $div = $('#container-gifs');
    $div.empty();
    $div.append(`<h5 class="page-headers"><span>Trending Gifs</span></h5>`);
    const id = $(ev.target).attr('#trending-btn');
    await getTrending(id);
  } catch (error) {
    console.error(error);
  }
};

// eslint-disable-next-line max-len
const pressTrend = () => $(document).on('click', '#trending-btn', callbackTrend);

export {pressTrend};
