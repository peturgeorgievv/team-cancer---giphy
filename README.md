# Team Cancer - Giphy

Designing and implementing a Gifs SPA application for searching and sharing funny gifs with your friends. The application is able to display the top trending gifs, upload gifs and much more! For our purposes we are going to use the Giphy API.