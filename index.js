import {searching} from './functions/search.js';
import {pressTrend} from './functions/trending.js';
import {getRandom} from './functions/random.js';
import {uploadGifs} from './functions/upload.js';
import {myGifs} from './functions/myGifs.js';
import {liked} from './functions/Liked.js';
import {favouritesLoad} from './functions/favourites.js';
import {refreshRandom} from './functions/iconRandom.js';


(() => {
  // Trending event
  pressTrend();
  // Searching event
  searching();
  // Uploading gif
  uploadGifs();
  // Random
  getRandom();
  // Uploaded - My Gifs
  myGifs();
  // Check for liked gifs
  liked();
  // Favaurites
  favouritesLoad();
  // Logo click
  refreshRandom();
})();
