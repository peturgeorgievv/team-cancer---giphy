* Must Requirements

    - Display Trending Gifs - https://developers.giphy.com/docs/api/endpoint/#trending
    - Search Gifs - https://developers.giphy.com/docs/api/endpoint/#search
    - Display Gif Details - https://developers.giphy.com/docs/api/endpoint/#get-gif-by-id
    - Upload Gif 
                    - https://developers.giphy.com/docs/api/endpoint/#upload
                    - https://developer.mozilla.org/en-US/docs/Web/API/FormData
                    - https://giphy.com/explore/channel

* Should Requirements
    - Display Uploaded Gifs 
                                - https://www.w3schools.com/jsref/prop_win_localstorage.asp
                                - https://developers.giphy.com/docs/api/endpoint/#get-gifs-by-id
    - Favorite Gif 
                    - https://www.w3schools.com/jsref/prop_win_localstorage.asp
                    - https://developers.giphy.com/docs/api/endpoint/#get-gif-by-id
                    - https://developers.giphy.com/docs/api/endpoint/#random

* General Requirements

- You must use jQuery for DOM manipulations and Ajax requests

- You must use Git to keep your source code and for team collaboration.

- You must use ESLint to write consistently styled code.

- You must use modules and they should be single-responsible.

- You must use correct naming and write clean, self-documenting code.

- You must have usable user interface

- You must use the latest features of ECMAScript.

- You could use external libraries such as Bootstrap, Materialize or other to style your project

- You could use external libraries such as jQuery UI, Kendo UI or other for custom controls

